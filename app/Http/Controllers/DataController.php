<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use JWTAuth;
use App\Imenik;

class DataController extends Controller
{

    public function getImenik()
    {
        if($user = JWTAuth::parseToken()->authenticate()) {
            $user_id = $user->id;
            $data = Imenik::all()
                ->where('user_id', $user_id);

            return response()->json(compact('data'),200);
        }


    }

    public function insertImenik(Request $request){
        if($user = JWTAuth::parseToken()->authenticate()) {
            $user_id = $user->id;
            $data = Imenik::create([
                'ime_kontakta' => $request->get('ime_kontakta'),
                'telefon' => $request->get('telefon'),
                'email' => $request->get('email'),
                'user_id' => $user_id
            ]);

            $msg = 'Uspesno ste dodali novi kontakt!';

            return response()->json(compact('msg'),200);
        }
    }

    public function editImenik(Request $request){
        if($user = JWTAuth::parseToken()->authenticate()) {
            $id = $request->get('id');
            $data = DB::table('imenik')
                ->where('id', $id)
                ->where('user_id', $user->id)
                ->update(['ime_kontakta' => $request->get('ime_kontakta')]);

            if($data){
                $msg = 'Uspesno ste izmenili kontakt!';
            }else {
                $msg = 'Error!';
            }

            return response()->json(compact('msg'),200);
        }
    }

}
