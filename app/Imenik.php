<?php

namespace App;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\Model;

class Imenik extends Model
{
    use Notifiable;

    protected $fillable = [
        'ime_kontakta', 'telefon', 'email', 'user_id',
    ];

    protected $table = 'imenik';
}
